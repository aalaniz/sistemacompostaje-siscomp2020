import RPi.GPIO as GPIO

class Sensor:
	GPIO.setmode(GPIO.BOARD) # Broadcom pin-numbering scheme

	def __init__(self, name,pin,optimum):
		self.name = name
		self.optimum = optimum
		self.pin = pin
		self.type_sensor = "digital"
		self.currentValue = 0
		GPIO.setup(self.pin, GPIO.IN)

	def getCurrentValue(self):
		return GPIO.input(self.pin)

	def getInfo(self):
		data = ""
		data=data+self.name
		data=data+","+str(self.pin)
		data=data+","+self.type_sensor
		data=data+","+str(self.optimum)
		data=data+","+str(self.getCurrentValue())

		return data

	def setPin(self,new_pin):
		self.pin = new_pin
		return 0

	def getType(self):
		return self.type_sensor
	
	def setType(self, new_type):
		if (new_type == 'analogic'):
			self.type_sensor = new_type
		else:
			self.type_sensor = 'digital'
		
		return 0

	def setOptimum(self, new_optimum):
		if(new_optimum.isdigit()):
			self.optimum = new_optimum
		else:
			return -1
		return 0
