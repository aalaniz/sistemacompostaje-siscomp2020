from flask import Flask, render_template, redirect, url_for, request
from sensor import Sensor
from motor import Motor
from valve import Valve
from orm import ORM

# import psutil
import datetime
# import ctypes

# _bl = ctypes.CDLL('./blink/blink2.so')
app = Flask(__name__)
valve_up=0
s_humedad = Sensor("sensor_humedad",8,50)
motor = Motor("led amarillo",10)
valve = Valve("led rojo",18)

devices=[s_humedad,motor,valve]

#auto_logger = ORM('./logs/auto_log.txt','auto',3600)
#auto_logger.initLog(devices)
activity_logger = ORM('./logs/activity_log.txt','manual',0)

def template(title = "Bienvenidos al sistema de monitoreo de compost!", text = "",valve_up = valve_up):
    now = datetime.datetime.now()
    timeString = now
    templateDate = {
        'title' : title,
        'time' : timeString,
        'text' : text,
        'valve_up' : valve_up
    }
    return templateDate

@app.route("/")
def hello():
    templateData = template()
    return render_template('main.html', **templateData)

@app.route("/sensors")
def info():
    array_info = s_humedad.getInfo()
    templateData = template(text = array_info)
    return render_template('main.html', **templateData)
    
@app.route("/sensors/<int:optimum>")
def setOpt(optimum):
    if(s_humedad.setOptimum(optimum) < 0):
        templateData = template(text = "Ingresar un valor mayor a 0")
        return render_template('main.html', **templateData)
    activity_logger.recordManual("Sensor humedad: Valor optimo = "+str(optimum)+".\n",datetime.datetime.now())
    templateData = template(text = s_humedad.name+": Valor cambiado con exito")
    return render_template('main.html', **templateData)
 
@app.route("/motor/on")
def move():
    motor.move()
    activity_logger.recordManual("Motor: Status = ON.\n",datetime.datetime.now())
    templateData = template(text = "Motor Prendido")
    return render_template('main.html', **templateData)
    
@app.route("/motor/off")
def stop():
    motor.stop()
    activity_logger.recordManual("Motor: Status = OFF.\n",datetime.datetime.now())
    templateData = template(text = "Motor Apagado")
    return render_template('main.html', **templateData)

# @app.route("/asm")
# def action_asm():
#     global _bl
#     _bl.main()
#     templateData = template(text = "Funcionalidad asm")
#     return render_template('main.html', **templateData)

@app.route("/valve")
def action_module():
    ret=valve.loadDriver()
    if(ret!=0):
        templateData = template(text = "Falla en la carga de modulo")
        return render_template('main.html', **templateData)
    templateData = template(text = "El modulo se cargo con exito",valve_up=1)
    valve_up=1
    return render_template('main.html', **templateData)
    

@app.route("/valve/<string:status>")
def action_module_time(status):
    if(status == 'on' and valve.changeStatus(status) == 0):
        activity_logger.recordManual("Valvula: Status = ON.\n",datetime.datetime.now())
        templateData = template(text = "Modulo on")
        return render_template('main.html', **templateData)
    elif (status== 'off' and valve.changeStatus(status) == 0):
        activity_logger.recordManual("Valvula: Status = OFF.\n",datetime.datetime.now())
        templateData = template(text = "Modulo off")
        return render_template('main.html', **templateData)
    else:
        templateData = template(text = "Ingresar 'on' u 'off' para cambiar el estado de la valvula")
        return render_template('main.html', **templateData)
        
@app.route("/valve/<int:time_on>")
def setTimeValve(time_on):
    if(valve.setTime_on(time_on) < 0):
        templateData = template(text = "Ingresar un valor mayor a 0 (tiempo en segundos)")
        return render_template('main.html', **templateData)
    activity_logger.recordManual("Valvula: Tiempo encendido = "+str(time_on)+".\n",datetime.datetime.now())
    templateData = template(text = valve.name+": Valor cambiado con exito")
    return render_template('main.html', **templateData)

# @app.route("/motor/auto/<toggle>")
# def auto_water(toggle):
#     running = False
#     if toggle == "ON":
#         templateData = template(text = "Auto Watering On")
#         for process in psutil.process_iter():
#             try:
#                 if process.cmdline()[1] == 'auto_water.py':
#                     templateData = template(text = "Already running")
#                     running = True
#             except:
#                 pass
#         if not running:
#             os.system("python3 auto_water.py&")
#     else:
#         templateData = template(text = "Auto Watering Off")
#         os.system("pkill -f water.py")

#     return render_template('main.html', **templateData)


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=7777, debug=True)
