#import RPi.GPIO as GPIO
import time

class ORM:
	#GPIO.setmode(GPIO.BOARD) # Broadcom pin-numbering scheme

	def __init__(self, path,mode,t):
		self.directoryLog = path
		self.mode = mode
		self.timeLog = t

	def saveInfo(self,devices):
		for dev in devices:
			try:
				f = open(self.directoryLog, "a")
				f.write(dev.getInfo())
				f.write("\n")
				f.close()
			except:
				return "NEVER!"
		return 0
		
	def setDirectoryLog(self,path):
		self.directoryLog = path
		return 0
		
	def initLog(self,devices):
		while 1:
			self.saveInfo(devices)
			print("Escrito en log auto")
			time.sleep(self.timeLog)
		return
	
	def recordManual(self,data,t):
		try:
			f = open(self.directoryLog, "a")
			f.write(str(t)+"---->")
			f.write(data)
			f.close()
		except:
			return -1
		
		return 0
