import RPi.GPIO as GPIO

class Motor:
	GPIO.setmode(GPIO.BOARD) # Broadcom pin-numbering scheme

	def __init__(self, name, pin):
		self.name = name
		self.status = 'off'
		self.time_on = 0
		self.pin = pin
		GPIO.setup(self.pin, GPIO.OUT)
		GPIO.output(self.pin, GPIO.LOW)

	def getInfo(self):
		data = ""
		data=data+self.name
		data=data+","+str(self.pin)
		data=data+","+self.status
		if (self.status == 'on'):
			data=data+","+self.time_on
		return data

	def setStatus(self,new_status):
		if (new_status == 'on'):
			self.status = new_status
		else:
			self.status = 'off'
		return 0

	def getTime_on(self):
		return self.time_on
	
	def setTime_on(self, new_time):
		if (new_time.isdigit()):
			self.time_on = new_time
		else:
			return -1		
		return 0

	def move(self):
		GPIO.output(self.pin, GPIO.HIGH)
		return 0
	
	def stop(self):
		GPIO.output(self.pin, GPIO.LOW)
		return 0
