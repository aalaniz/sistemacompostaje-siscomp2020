#include "mod_driver.h"
#define BUF_LEN 12
#define DEVICE_NAME "driverSiscomp"
#define SUCCESS 0

MODULE_LICENSE("GPL");

static char drv_buffer[BUF_LEN];
static unsigned long drv_buf_size = 0;
static int mod_major;
static int device_open=0;
static struct gpio port[] = {
	{ 18, GPIOF_OUT_INIT_LOW, "Electro_Valvula" }
};
static struct class *classD; 
static struct device *modulo_ledDev;

static int __init mod_init(void){
	int major, ret;

	printk(DEVICE_NAME ": Iniciando.\n");

	major = register_chrdev(0, DEVICE_NAME, &this_fops);
	if (major>0){
		printk(DEVICE_NAME ": Módulo registrado con major=%d.\n",major);
		mod_major = major;
	}

	classD = class_create(THIS_MODULE, "Modulo_Valvula");
	if (IS_ERR(classD)) {
		unregister_chrdev(mod_major, "modulo_valvula");
		return PTR_ERR(classD); 
	} 

	modulo_ledDev = device_create(classD, NULL, MKDEV(mod_major, 0), NULL, "modulo_valvula");
	if(IS_ERR(modulo_ledDev)){
		printk(KERN_INFO "Error creando dispositivo \n");
		class_destroy (classD);
		unregister_chrdev(mod_major, "modulo_valvula");
		return PTR_ERR(classD); 
	}

	ret = gpio_request_array(port, ARRAY_SIZE(port));

        if (ret) {
                printk(KERN_ERR "request GPIOs desconocida: %d\n", ret);
                return ret;
        }

	return SUCCESS;
}

static void __exit mod_exit(void){
	device_destroy(classD, MKDEV(mod_major, 0) );
	class_destroy(classD);

	unregister_chrdev(mod_major, DEVICE_NAME);

    gpio_set_value(port[0].gpio, 0);

    gpio_free_array(port, ARRAY_SIZE(port));

	printk(DEVICE_NAME ": Módulo desregistrado.\n");
	printk(DEVICE_NAME ": Saliendo.\n");
}

static int mod_open(struct inode *inode, struct file *file){
    printk(DEVICE_NAME": Abriendo el módulo.\n");

	if (device_open)
		return -EBUSY;

	device_open++;

	try_module_get(THIS_MODULE);	// Incrementa el conteo de uso de procesos que usan el modulo

	return SUCCESS;
}

static int mod_release(struct inode *inode, struct file *file){
	printk(DEVICE_NAME": Cerrando el módulo.\n");

	device_open--;
	module_put(THIS_MODULE);	// Decrementa el conteo de uso de procesos que usan el modulo

	return SUCCESS;
}

static ssize_t mod_read(struct file *file, char __user *buffer, size_t len, loff_t *offset){
	int ret;
	
	printk(DEVICE_NAME": Leyendo en el módulo.\n");
	//printk(drv_buf_size_read);

	copy_to_user(buffer, drv_buffer, drv_buf_size); // to, from
	ret = drv_buf_size;
	
	if (*offset == 0) {
		*offset+=1;
		return ret;
	}
	return 0;
}

static ssize_t mod_write(struct file *file,const char __user *buffer, size_t len, loff_t *offset){
	
	char tmp[len];
	printk(DEVICE_NAME": Escribiendo en el módulo.\n");

	drv_buf_size = len;
	
	// se acota el buffer de entrada desde el usuario
	if(drv_buf_size > BUF_LEN){
		drv_buf_size = BUF_LEN;
	}
	
	if(copy_from_user(tmp, buffer, drv_buf_size)){
		return -EFAULT;		// es un error porque es invalido el puntero a la memoria de usuario o porque una dir de memoria durante el copiado lo es.
	}
	
	char on1[3]= "on\0";
	char off1[4]= "off\0";
	
	if(len == 3){
		printk(DEVICE_NAME": Entro al on(len).\n");
		if(tmp[0]==on1[0] && tmp[1]==on1[1]){
			char on[3]="ON\0";
			memcpy(drv_buffer, on, drv_buf_size);
			gpio_set_value(port[0].gpio, 1);
			printk(DEVICE_NAME": Entro al on.\n");
		}
	}
	if(len == 4){
		printk(DEVICE_NAME": Entro al off(len).\n");
		if(tmp[0]==off1[0] && tmp[1]==off1[1] && tmp[2]==off1[2]){
			char off[4]="OFF\0";
			memcpy(drv_buffer, off, drv_buf_size);
			gpio_set_value(port[0].gpio, 0);
			printk(DEVICE_NAME": Entro al off.\n");
		}
	}
	
	return drv_buf_size;
}


