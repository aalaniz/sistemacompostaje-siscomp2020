#include <linux/module.h>
#include <linux/fs.h>
#include <linux/uaccess.h>
#include <linux/err.h>
#include <linux/slab.h>

#include <linux/kernel.h>

#include <linux/device.h> 
#include <linux/kdev_t.h>
#include <linux/cdev.h>
#include <linux/printk.h>

#include <linux/delay.h>
#include <linux/interrupt.h>
#include <linux/gpio.h>

static int mod_open(struct inode *inode, struct file *file);
static int mod_release(struct inode *inode, struct file *file);
static ssize_t mod_read(struct file *file, char __user *buffer, size_t len, loff_t *offset);
static ssize_t mod_write(struct file *file, const char __user *buffer, size_t len, loff_t *offset);

struct file_operations this_fops = {
    .open       = mod_open,
    .release    = mod_release,
    .read       = mod_read,
    .write      = mod_write
};

static int __init mod_init(void);
static void __exit mod_exit(void);
module_init(mod_init);
module_exit(mod_exit);


