import RPi.GPIO as GPIO
import os
import time

class Valve:
	GPIO.setmode(GPIO.BOARD) # Broadcom pin-numbering scheme

	def __init__(self, name, pin):
		self.name = name
		self.status = 'off'
		self.time_on = 0 #en segundos
		self.pin = pin
		GPIO.setup(self.pin, GPIO.OUT)
		GPIO.output(self.pin, GPIO.LOW)

	def getInfo(self):
		data = ""
		data=data+self.name
		data=data+","+str(self.pin)
		data=data+","+self.status
		if (self.status == 'on'):
			data=data+","+self.time_on
		return data

	def loadDriver(self):
		myPass='123456789'
		s="echo %s | sudo -S insmod ./driver/mod_driver.ko" % (myPass)
		ret=os.system(s)
		return ret
		
	def changeStatus(self,new_status):
		if (new_status == 'on' or new_status == 'off'):
			self.status = new_status
			myPass='123456789'
			chmod = "echo %s | sudo -S chmod 777 /dev/modulo_valvula" % (myPass)
			os.system(chmod)
			s="echo %s | sudo -S echo %s > /dev/modulo_valvula" % (myPass,new_status)
			os.system(s)
			if(new_status == 'on'):
				time.sleep(self.time_on)
				s="echo %s | sudo -S echo %s > /dev/modulo_valvula" % (myPass,'off')
				os.system(s)
			return 0
		else:
			self.status = 'off'
		return -1

	def getTime_on(self):
		return self.time_on
	
	def setTime_on(self, new_time):
		if (new_time>0):
			self.time_on = new_time
		else:
			return -1		
		return 0


